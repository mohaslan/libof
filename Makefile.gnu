CFLAGS+= -Wall -I/usr/include/kqueue -I. -fPIC -g
LDFLAGS+= -lkqueue -L. -lbsd -lhashtab -lpthread
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: libof.a libof.so test
%.o: %.c 
	$(CC) -o $@ -c $< $(CFLAGS)
libof.a: $(OBJS)
	ar cq libof.a utils.o libof.o of10.o
libof.so: $(OBJS)
	cc -shared -o libof.so utils.o libof.o of10.o
test: libof.a
	$(CC) test.o -o test -l:libof.a $(LDFLAGS)
install: libof.a libof.so
	install -m 0644 libof.a /usr/lib/
	install -m 0644 libof.so /usr/lib/
	install -m 0644 libof.h /usr/include/
	install -m 0644 of10.h /usr/include/
clean:
	rm -f *.o *.a *.so test
