/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>
#include <time.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/event.h>
#include <netinet/in.h>

#include "libof.h"
#include "hashtab.h"

#define MAXTIMERS	100
#define TCPBACKLOG	500
#define HEARTBEAT	5


#ifdef DEBUG
static void
of_print_header(struct ofp_header *hdr)
{
	if (hdr == NULL)
		return;
        printf("header -> {version = %d, type = %d, length = %d, xid = %x}\n",
                (unsigned int)hdr->version,
                (unsigned int)hdr->type,
                (unsigned int)be16toh(hdr->length),
                (unsigned int)be32toh(hdr->xid));
}
#endif

static void
sendtosocket(int skt, struct ofp_header *hdr)
{
	assert(be16toh(hdr->length) >= sizeof(struct ofp_header));
	if (be16toh(hdr->length) >= sizeof(struct ofp_header))
		write(skt, hdr, (size_t)be16toh(hdr->length));	// FIXME: error check
}

static void
send_msg(struct of_controller *controller, struct of_dataplane *dp, struct ofp_header *hdr)
{
	if (controller == NULL || dp == NULL || hdr == NULL)
		return;
	sendtosocket(dp->socket, hdr);	
}

static void
heartbeat(struct of_controller *controller)
{
	size_t iter, ksz, vsz;
	time_t now;
	uintptr_t *ptr;
	uint64_t *dpid;
	struct of_dataplane *dp;

	now = time(NULL);
	/* TODO: something smarter */
	HASHTAB_FOREACH(controller->dpids, iter, dpid, ksz, ptr, vsz) {
		hashtab_at(&(controller->dpids), iter, (void **)(&dpid), &ksz, (void **)(&ptr), &vsz);
		dp = (struct of_dataplane *)(*ptr);
		if (dp->ready && (now >= (dp->lastmsg_ts + HEARTBEAT))) {
#ifdef DEBUG
			printf("last pinged %ju, now is %ju (%ju)\n", dp->lastmsg_ts, now, (now - dp->lastmsg_ts));
			printf("heartbeating: dpid 0x%llx\n", *dpid);
#endif
			dp->protocol->ping(controller, dp);
		}
	}
}

static void
timer(struct of_controller *controller, unsigned int period, void (*callback)(struct of_controller *))
{
	int tid;
	struct kevent kev;
	uintptr_t ptr;

	if (controller->n_timers >= MAXTIMERS)
		return;
	tid = controller->n_timers;
	ptr = (uintptr_t)callback;
	EV_SET(&kev, tid, EVFILT_TIMER, EV_ADD | EV_ENABLE, 0, period, NULL);
	if ((kevent(controller->ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		errx(1, "timer");
	/*printf("callback: 0x%llx (%zu)\n", ptr, sizeof(uintptr_t));*/
	if (!hashtab_put(&(controller->timer_callbacks), (void *)(&tid), sizeof(int), (void *)(&ptr), sizeof(uintptr_t)))
		errx(1, "hashtab_put");
	controller->n_timers++;
}

static void
handler(struct of_controller *controller, void (*callback)(struct of_controller *, struct of_event *))
{
	controller->of_handler = callback;
}

static void
event_loop(struct of_controller *controller)
{
	char buf[BUFSIZ + 1];
	size_t sz;
	ssize_t bytes;
	int n, s;
	struct kevent kev;
	struct sockaddr_in sa;
	struct sockaddr_storage sw_addr;
	struct of_dataplane *dp;
	struct ofp_header *recv;
	socklen_t addr_len = sizeof(struct sockaddr_storage);
	uintptr_t p, *ptr;
	void (*callback)(struct of_controller *);

	if (controller == NULL)
		return;
	if ((controller->socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return;
	memset(&sa, 0, sizeof sa);
	sa.sin_family = AF_INET;
	sa.sin_port = htons(controller->port);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(controller->socket, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
		close(controller->socket);
		return;
	}

	if (listen(controller->socket, TCPBACKLOG) == -1) {
		close(controller->socket);
		return;
	}

	EV_SET(&kev, controller->socket, EVFILT_READ, EV_ADD, 0, 0, NULL);
	if ((kevent(controller->ev_queue, &kev, 1, NULL, 0, NULL)) == -1) {
		close(controller->socket);
		return;
	}

	while(1) {
		n = kevent(controller->ev_queue, NULL, 0, &kev, 1, NULL);	/* TODO: use bigger eventlist */
		if (n == -1)
			errx(1, "event_loop");
		else if (n == 0)
			continue;
		if (kev.filter == EVFILT_TIMER) {	/* timers */
			s = (int)(kev.ident);	/* uintptr_t != int */
			if (!hashtab_get(&(controller->timer_callbacks), (void *)(&s), sizeof(int), (void **)(&ptr), &sz))
				errx(1, "hashtab_get");
			callback = (void (*)(struct of_controller *))(*ptr);
			//printf("callback: 0x%llx (%zu) for timer %d\n", callback, sz, s);
			callback(controller);
		} else if (kev.filter == EVFILT_READ) {	/* sockets */
			s = (int)(kev.ident);	/* uintptr_t != int */
			if (s == controller->socket) {	/* server socket: new connection */
				dp = (struct of_dataplane *)calloc(1, sizeof(struct of_dataplane));
				dp->socket = accept(controller->socket, (struct sockaddr *)&sw_addr, &addr_len);
				if (fcntl(dp->socket, F_SETFL, O_NONBLOCK) == -1)
					errx(1, "fcntl");
				p = (uintptr_t)dp;
				if (!hashtab_put(&(controller->conns), (void *)(&(dp->socket)), sizeof(dp->socket), (void *)(&p), sizeof(uintptr_t)))
					errx(1, "hashtab_put");
				printf("new connection.\n");
				EV_SET(&kev, dp->socket, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, NULL);
				if ((kevent(controller->ev_queue, &kev, 1, NULL, 0, NULL)) == -1) {
					close(dp->socket);
					return;
				}
				controller->protocol->handshake(controller, dp); 
			} else {	/* a client socket */
				bytes = read(s, buf, sizeof(buf));
				if (bytes == 0) {
					printf("connection closed.\n");
					EV_SET(&kev, s, EVFILT_READ, EV_DELETE, 0, 0, NULL);
					if ((kevent(controller->ev_queue, &kev, 1, NULL, 0, NULL)) == -1) {
						close(s);
						return;
					}
					close(s);
					/* TODO: remove from hashtabs and free structs */
				}
				else if (bytes < sizeof(struct ofp_header)) {
					printf("error! %zd\n", bytes);
					return;
				}
				else {
					if (!hashtab_get(&(controller->conns), (void *)&s, sizeof(s), (void **)(&ptr), &sz))
						errx(1, "hashtab_get");
					assert(sz == sizeof(uintptr_t));
					dp = (struct of_dataplane *)(*ptr);
#ifdef DEBUG
					printf("read %zd bytes.\n", bytes);
#endif
					recv = (struct ofp_header *)buf;
					dp->lastmsg_ts = time(NULL);
					controller->protocol->recv(controller, dp, recv);
				}
			}
		}
	}
}

int
of_controller_init(struct of_controller *controller, int port, const struct of_protocol *proto)
{
	if ((controller->socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		return 1;
	controller->port = port;
	controller->n_timers = 0;
	controller->protocol = proto;
	controller->send = send_msg;
	controller->loop = event_loop;
	controller->timer = timer;
	controller->handler = handler;
	controller->of_handler = NULL;
	if ((controller->ev_queue = kqueue()) == -1)
		errx(1, "kqueue");
	if (!hashtab_init(&(controller->conns), 8, NULL))
		errx(1, "hashtab_init");
	if (!hashtab_init(&(controller->dpids), 8, NULL))
		errx(1, "hashtab_init");
	if (!hashtab_init(&(controller->timer_callbacks), 8, NULL))
		errx(1, "hashtab_init");
	controller->timer(controller, 3000, heartbeat);
	return 0;
}

