/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <err.h>

#include "libof.h"
#include "of10.h"

static void of10_init(struct of_controller *);
static int of10_handshake(struct of_controller *, struct of_dataplane *);
static int of10_recv(struct of_controller *, struct of_dataplane *, struct ofp_header *);
static int of10_ping(struct of_controller *, struct of_dataplane *);

static struct of_protocol ofp10 = {
	.version = OFP_VERSION_10,
	.init = of10_init,
	.handshake = of10_handshake,
	.recv = of10_recv,
	.ping = of10_ping
};

static void
of10_print_header(const struct ofp_header *hdr)
{
	printf("header -> {version = %d, type = %d, length = %hu, xid = %x}\n",
		(unsigned int)hdr->version,
		(unsigned int)hdr->type,
		be16toh(hdr->length),
		(unsigned int)be32toh(hdr->xid));
}

static uint64_t
xid()
{
	return (uint64_t)((uint64_t)arc4random() << 32 | arc4random());
}

static int
handle_echo_request(struct of_controller *ctl, struct of_dataplane *dp, const struct ofp_header *msg_in)
{
	struct ofp_header msg_out;

	/* send ECHO_REPLY */
	msg_out.version = 0x1;
	msg_out.type = OFPT10_ECHO_REPLY;
	msg_out.length = htobe16(sizeof(struct ofp_header));
	msg_out.xid = msg_in->xid;
	printf("sending: ");
	of10_print_header(&msg_out);
	ctl->send(ctl, dp, &msg_out);
	return 1;
}

static int
handle_features_reply(struct of_controller *ctl, struct of_dataplane *dp, const struct ofp_header *msg)
{
	uintptr_t ptr;
	uint64_t dpid = 0;

	ptr = (uintptr_t)dp;
	dp->sw_features = (struct ofp_header *)malloc(sizeof(struct of10_switch_features));
	if (dp->sw_features == NULL)
		return 0;
	memcpy((void *)(dp->sw_features), msg, sizeof(struct of10_switch_features));
	dpid = be64toh(((struct of10_switch_features *)(dp->sw_features))->datapath_id);
	if (!hashtab_put(&(ctl->dpids), (void *)(&dpid), sizeof(uint64_t), (void *)(&ptr), sizeof(uintptr_t)))
		errx(1, "hashtab_put");
#ifdef DEBUG
	printf("datapath id: 0x%llx\n", dpid);
#endif
	return 1;
}

static int
send_features_request(struct of_controller *ctl, struct of_dataplane *dp)
{
	struct ofp_header msg;

	msg.version = OFP_VERSION_10;
	msg.type = OFPT10_FEATURES_REQUEST;
	msg.length = htobe16(sizeof(struct ofp_header));
	msg.xid = xid();
	printf("sending: ");
	of10_print_header(&msg);
	ctl->send(ctl, dp, &msg);
	return 1;
}

static void
of10_init(struct of_controller *ctl)
{

}

static int
of10_handshake(struct of_controller *ctl, struct of_dataplane *dp)
{
	struct ofp_header msg;

	/* send HELLO */
	msg.version = 0x1;
	msg.type = OFPT10_HELLO;
	msg.length = htobe16(sizeof(struct ofp_header));
	msg.xid = xid();
	printf("sending: HELLO ");
	of10_print_header(&msg);
	ctl->send(ctl, dp, &msg);
	return 1;
}

static int
of10_ping(struct of_controller *ctl, struct of_dataplane *dp)
{
	struct ofp_header msg;

	/* send ECHO_REQUEST */
	msg.version = 0x1;
	msg.type = OFPT10_ECHO_REQUEST;
	msg.length = htobe16(sizeof(struct ofp_header));
	msg.xid = xid();
	printf("sending: EHCO_REQUEST ");
	of10_print_header(&msg);
	ctl->send(ctl, dp, &msg);
	return 1;
}

static int
of10_recv(struct of_controller *ctl, struct of_dataplane *dp, struct ofp_header *msg)
{
	struct of_event	*ev;

	if (msg == NULL)
		return 0;
	ev = (struct of_event *)malloc(sizeof(struct of_event));
	ev->dp = dp;

#ifdef DEBUG
	of10_print_header(msg);
#endif
	switch (msg->type) {
	case OFPT10_HELLO:
#ifdef DEBUG
		printf("OFPT_HELLO.\n");
#endif
		/* TODO: check for supported OpenFlow version */
		dp->protocol = of10_protocol();
		/* assume we already sent our hello message during handsaking via of10_init() */
		if (!send_features_request(ctl, dp))
			return 0;
		break;
	case OFPT10_OFPT_ERROR:
#ifdef DEBUG
		printf("OFPT_ERROR.\n");
#endif
		break;
	case OFPT10_ECHO_REQUEST:
#ifdef DEBUG
		printf("OFPT_ECHO_REQUEST.\n");
#endif
		if (!handle_echo_request(ctl, dp, msg))
			return 0;
		break;
	case OFPT10_ECHO_REPLY:
#ifdef DEBUG
		printf("OFPT_ECHO_REPLY.\n");
#endif
		break;
	case OFPT10_FEATURES_REQUEST:
#ifdef DEBUG
		printf("XXX OFPT_FEATURES_REQUEST.\n");
#endif
		break;
	case OFPT10_FEATURES_REPLY:
#ifdef DEBUG
		printf("OFPT_FEATURES_REPLY.\n");
#endif
		if (!handle_features_reply(ctl, dp, msg))
			break;
		dp->ready = 1;
		/* generate a connection up event */
		ev->type = OFEV_CONNECTION_UP;
		if (ctl->of_handler != NULL)
			ctl->of_handler(ctl, ev);
		break;
	case OFPT10_PACKET_IN:
#ifdef DEBUG
		printf("OFPT_PACKET_IN.\n");
#endif
		/* generate a protocol message event */
		ev->type = OFEV_PROTO_MESSAGE;
		ev->ofp_hdr = msg;
		if (dp->ready && ctl->of_handler != NULL)
			ctl->of_handler(ctl, ev);
		break;
	case OFPT10_STATS_REPLY:
#ifdef DEBUG
		printf("OFPT_STATS_REPLY.\n");
#endif
		/* generate a protocol message event */
		ev->type = OFEV_PROTO_MESSAGE;
		ev->ofp_hdr = msg;
		if (dp->ready && ctl->of_handler != NULL)
			ctl->of_handler(ctl, ev);
	default:
#ifdef DEBUG
		printf("XXX UNSUPPORTED MESSAGE TYPE.\n");
#endif
		break;
	}
	free(ev);
	return 1;
}

const struct of_protocol *
of10_protocol()
{
	return &ofp10;
}


